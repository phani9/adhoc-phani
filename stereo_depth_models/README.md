This repo contains models, scripts to evaluate the 3 stereo depth algorithms PSMNET, ANYNET, RTStereo trained on inhouse dataset from realsense

Anynet requires compilation of the spn module but otherwise the folder is self contained and does not need need any setup apart from pytorch, torchvision and opencv.
