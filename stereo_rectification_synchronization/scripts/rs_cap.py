import time
import pyrealsense2 as rs
import uuid

import cv2
from uuid import uuid1
import numpy as np
import os


left_cam_id = 2
right_cam_id =0

left = cv2.VideoCapture(left_cam_id)
right = cv2.VideoCapture(right_cam_id)

left.set(cv2.CAP_PROP_FPS, 30)
left.set(4, 480)
left.set(3, 640)

right.set(4, 480)
right.set(3, 640)
right.set(cv2.CAP_PROP_FPS, 30)

config = rs.config()
config.enable_stream(rs.stream.depth, 848, 480, rs.format.z16, 30)
config.enable_stream(rs.stream.color, 848, 480, rs.format.bgr8, 30)

pipeline = rs.pipeline()
profile = pipeline.start(config)
depth_sensor = profile.get_device().first_depth_sensor()
depth_scale = depth_sensor.get_depth_scale()
depth_sensor.set_option(rs.option.emitter_enabled, 1)
depth_sensor.set_option(rs.option.emitter_on_off, 0);
depth_sensor.set_option(rs.option.enable_auto_exposure,1)
preset=1
depth_sensor.set_option(rs.option.visual_preset, preset)
ALIGN=0
dmax=5
scale_factor = 60000/dmax
print (preset, depth_sensor.get_option_value_description(rs.option.visual_preset, preset))

disparity_to_depth = rs.disparity_transform(True)
depth_to_disparity = rs.disparity_transform(False)
spatial = rs.spatial_filter()
spatial.set_option(rs.option.filter_magnitude, 3)
spatial.set_option(rs.option.filter_smooth_alpha, 1)
spatial.set_option(rs.option.filter_smooth_delta, 50)
temporal = rs.temporal_filter()
hole_filling = rs.hole_filling_filter()

align_to = rs.stream.color
align = rs.align(align_to)


def rs_frames_to_images(frames):
     color_frame = frames.get_color_frame()
     depth_frame = frames.get_depth_frame()
     color_frame = frames.get_color_frame()
     depth_frame = frames.get_depth_frame()
     #depth_frame = disparity_to_depth.process(depth_frame)
     depth_frame = spatial.process(depth_frame)
     depth_frame = temporal.process(depth_frame)
     #depth_frame = hole_filling.process(depth_frame)
     #depth_frame = depth_to_disparity.process(depth_frame)
     if not depth_frame or not color_frame:
         return -1, None, None

     #depth_frame = colorizer.colorize(depth_frame)
     depth_image = np.asanyarray(depth_frame.get_data())
     depth_image = depth_image*depth_scale
     depth_image[depth_image>dmax] = dmax
     #print (np.max(depth_image), depth_scale)
     depth_image = (depth_image*scale_factor).astype(np.uint16)

     color_image = np.asanyarray(color_frame.get_data())
     return 0, depth_image, color_image


print ('Space to save; Esc to quit')
left_dir = '/tmp/data/image_2/'
os.system('mkdir -p ' + left_dir)
right_dir = '/tmp/data/image_3/'
os.system('mkdir -p ' + right_dir)
depth_dir = '/tmp/data/disp_occ_0/'
os.system('mkdir -p ' + depth_dir)

depth_dir_aligned = '/tmp/data/disp_occ_0_aligned/'
os.system('mkdir -p ' + depth_dir_aligned)

rgb_dir = '/tmp/data/rgb/'
os.system('mkdir -p ' + rgb_dir)

while True:
                frames = pipeline.wait_for_frames()
                left.grab()
                right.grab()

                _,left_image = left.retrieve()
                _,right_image = right.retrieve()
                cv2.imshow('left', left_image)
                key = cv2.waitKey(1)
                if key == -1:
                    time.sleep(0.1)
                    continue
                if key == 27:
                    print ('Pressed Esc; quitting')
                    break
                if key != 32:
                    print ('Pressed ' + chr(key)  + ' ; Press space to save; esc to quit  ')
                    time.sleep(0.1)
                    continue
                print ('Pressed space ; saving !')

                # Align the depth frame to color frame
                aligned_frames = align.process(frames)

                # Get aligned frames
                #aligned_depth_frame = aligned_frames.get_depth_frame() # aligned_depth_frame is a 640x480 depth image
                #color_frame = aligned_frames.get_color_frame()
                ret_val, depth_image, color_image = rs_frames_to_images(frames)
                if ret_val<0:
                    continue
                _, depth_image_aligned, color_image = rs_frames_to_images(aligned_frames)

                '''
                #depth_frame = disparity_to_depth.process(depth_frame)
                depth_frame = spatial.process(depth_frame)
                depth_frame = temporal.process(depth_frame)
                depth_frame = hole_filling.process(depth_frame)
                #depth_frame = depth_to_disparity.process(depth_frame)
                if not depth_frame or not color_frame:
                    continue

                #depth_frame = colorizer.colorize(depth_frame)
                depth_image = np.asanyarray(depth_frame.get_data())
                depth_image = depth_image*depth_scale
                depth_image[depth_image>dmax] = dmax
                #print (np.max(depth_image), depth_scale)
                depth_image = (depth_image*scale_factor).astype(np.uint16)

                color_image = np.asanyarray(color_frame.get_data())
                '''


                name = str(uuid.uuid1())+ '.png'
                oname = left_dir + name
                cv2.imwrite(oname, left_image)

                oname = right_dir + name
                cv2.imwrite(oname, right_image)

                oname = depth_dir + name
                cv2.imwrite(oname,depth_image)

                oname = depth_dir_aligned + name
                cv2.imwrite(oname,  depth_image_aligned)

                oname = rgb_dir + name
                cv2.imwrite(oname,  color_image)
                print ('saving '  + oname)
                time.sleep(.1)
pipeline.stop()
left.release()
right.release()
