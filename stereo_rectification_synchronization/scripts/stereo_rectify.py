import numpy as np
import cv2
import argparse
import sys
import glob
from random import randint

def load_stereo_coefficients(path):
    """ Loads stereo matrix coefficients. """
    # FILE_STORAGE_READ
    cv_file = cv2.FileStorage(path, cv2.FILE_STORAGE_READ)

    # note we also have to specify the type to retrieve other wise we only get a
    # FileNode object back instead of a matrix
    K1 = cv_file.getNode("K1").mat()
    D1 = cv_file.getNode("D1").mat()
    K2 = cv_file.getNode("K2").mat()
    D2 = cv_file.getNode("D2").mat()
    R = cv_file.getNode("R").mat()
    T = cv_file.getNode("T").mat()
    E = cv_file.getNode("E").mat()
    F = cv_file.getNode("F").mat()
    R1 = cv_file.getNode("R1").mat()
    R2 = cv_file.getNode("R2").mat()
    P1 = cv_file.getNode("P1").mat()
    P2 = cv_file.getNode("P2").mat()
    Q = cv_file.getNode("Q").mat()

    cv_file.release()
    return [K1, D1, K2, D2, R, T, E, F, R1, R2, P1, P2, Q]

def rectify(leftFrame, rightFrame, params):
        K1, D1, K2, D2, R, T, E, F, R1, R2, P1, P2, Q = params[:]
        height, width = leftFrame.shape[:2]

        leftMapX, leftMapY = cv2.initUndistortRectifyMap(K1, D1, R1, P1, (width, height), cv2.CV_32FC1)
        left_rectified = cv2.remap(leftFrame, leftMapX, leftMapY, cv2.INTER_LINEAR, cv2.BORDER_CONSTANT)

        rightMapX, rightMapY = cv2.initUndistortRectifyMap(K2, D2, R2, P2, (width, height), cv2.CV_32FC1)
        right_rectified = cv2.remap(rightFrame, rightMapX, rightMapY, cv2.INTER_LINEAR, cv2.BORDER_CONSTANT)
        return left_rectified, right_rectified


#cv2.imshow('left',lrect)
#cv2.imshow('right', rrect)
#cv2.waitKey(0)
cal_file = 'stereo_cam.yml'
params = load_stereo_coefficients(cal_file)
left = cv2.imread('/tmp/left.png')
right = cv2.imread('/tmp/right.png')

lrect, rrect = rectify(left, right, params)
colors = [(0, 0, 255),(0, 255, 0), (255, 0,0), (255, 255, 0), (255, 0, 255), (0, 0, 255), (128, 255, 128), (128, 128, 255)]
out = np.hstack((lrect, rrect))
ncols = left.shape[1]*2
nrows = left.shape[0]
for i in range(0, (nrows//50)-1):
    c_ind = randint(0, 7)
    out = cv2.line(out, (0, i*50), (ncols-1, i*50),colors[c_ind],3)

cv2.imshow('c',out)
cv2.waitKey(00)
#cv2.imwrite('left_rectified.png',lrect)
#cv2.imwrite('right_rectified.png',rrect)
cal_file = 'stereo_cam.yml'
params = load_stereo_coefficients(cal_file)

left_dir = '/tmp/images/rectified/image_2/'
left_dir = '/media/lonewolf/9b5821bc-0e7a-4cf5-a842-61b5d46ca7743/work/clutterbot/data/inhouse_data/rpi_sets/set2/rectified/image_2/'
for lname in glob.glob(left_dir + '/*'):
    left = cv2.imread(lname)

    rname = lname.replace('image_2','image_3')
    right = cv2.imread(rname)

    lrect, rrect = rectify(left, right, params)
    cv2.imwrite(lname, lrect)
    cv2.imwrite(rname, rrect)
    print (rname)
